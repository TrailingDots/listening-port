# To use:
#    make output/README.html
# Results are in output/README.html
#
# This makefile takes ALL *.md files and creates
# html, pdf or epub formatted files in the
# output directory.
#
.PHONY: phony

FIGURES = $(shell find . -name '*.svg')

PANDOCFLAGS = -s -t html             \
  --table-of-contents                \
  --from=markdown                    \
  --number-sections                  \
  --indented-code-classes=javascript \
  --highlight-style=monochrome       \
  -V mainfont="Palatino"             \
  -V documentclass=report            \
  -V papersize=A5                    \
  -V geometry:margin=1in

all: phony output/book.pdf

output/%.html: %.md $(FIGURES) Makefile title.txt | output
		pandoc $< title.txt -o $@ $(PANDOCFLAGS)

output/%.pdf: %.md $(FIGURES) Makefile | output
		pandoc $< title.txt -o $@ $(PANDOCFLAGS)

output/%.epub: %.md $(FIGURES) Makefile | output
		pandoc $< title.txt -o $@ $(PANDOCFLAGS)

output:
		mkdir ./output

clean: phony
		rm -rf ./output

open: phony output/book.pdf
	open output/book.pdf
